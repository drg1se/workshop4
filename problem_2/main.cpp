/// Recreate the multiplication table task, allowing the user to rerun the program again after it prints the multiplication table.
#include <iostream>
using namespace std;

void mult_table(int n, int m)
{
    for (int i = 0; i < n; i++)
        cout << '\t' << i + 1;
    for (int i = 1; i <= n; i++)
    {
        cout << endl << i;
        for (int j = 1; j <= n; j++)
            cout << '\t' << (i*j) % m;
    }
}

int main()
{
    int a = 1, b;
    cout << "Enter the parameters(size and modulo) to run the program and 0 to stop the program: ";
    cin >> a;
    while (a)
    {
        cin >> b;
        cout << endl;
        mult_table(a, b);
        cout << endl;
        cout << "Enter the parameters(size and modulo) to run the program and 0 to stop the program: ";
        cout << endl;
        cin >> a;
    }
    return 0x0;
}