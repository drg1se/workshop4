/// Implement strlen function: count the length of a C-style string variable initialized with a string literal

#include <iostream>

int custom_strlen (const char string[])
{
  int out=1;
  while (string[out] != '\0')
      out++;
  return out;
}

int main()
{

    std::cout << custom_strlen ("Hello World") << std::endl;
    return 0;
}
